package ftp;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class FtpClient {
	BufferedReader commandsIn;
	BufferedWriter commandsOut;
	Socket dataSocket;
	Socket command;
	ServerSocket serverSocketData;
	static int commandPORT; // 21 default
	static int dataPort;
	String serverReply;
	BufferedReader dataReader;
	FileWriter fstream;
	BufferedWriter writeToFile;

	public static void main(String[] args) {
		FtpClient client = new FtpClient();
		if (args.length == 2) {
			client.start(args[0], args[1]);

		} else {
			System.err
					.println("Incorrect number of command line arguments, input must be either host, username, password OR just host for anonymous ftp connections");
			System.exit(0);
		}

	}

	public FtpClient() {

	}

	public void connect(String host, String port) throws Exception {
		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));
		String[] replySplit;
		InetAddress addr = InetAddress.getByName(host);
		System.out.println(addr);
		commandPORT = Integer.parseInt(port);

		command = new Socket(addr, commandPORT);
		dataPort = command.getLocalPort() + 1;
		serverSocketData = new ServerSocket(dataPort);
		commandsOut = new BufferedWriter(new OutputStreamWriter(
				command.getOutputStream()));

		commandsIn = new BufferedReader(new InputStreamReader(
				command.getInputStream()));
		serverReply = getResponse();
		replySplit = serverReply.split(" ", 2);
		// check for reply from server to ensure connection is ok
		if (replySplit[0].charAt(0) == '2') {

			// send the user name and check it is ok with the server
			System.out.print("Enter username: ");
			String username = stdIn.readLine();

			send("USER " + username);
			serverReply = getResponse();

			if (serverReply.startsWith("331")) {

				// username ok
				System.out.println("Enter password for " + username + ": ");
				String password = stdIn.readLine();
				send("PASS " + password);
				serverReply = getResponse();
				if (!serverReply.startsWith("2")) {

					System.err.println("Server failed to authenticate user: ");
					System.exit(0);

				}
			} else if (serverReply.startsWith("230")) {
				// dont send it a password it is not needed

			} else {
				// server rejected username

				System.err
						.println("Server rejected the username on connecting ");

				System.exit(0);
			}

		} else {

			System.err.println("Connection refused by server. Message: "
					+ serverReply);
			System.exit(0);
		}

	}

	public void start(String host, String port) {

		BufferedReader stdIn = new BufferedReader(new InputStreamReader(
				System.in));

		String userInput;
		String[] uInputSplit;

		try {
			// connect to the server socket on default ftp port 21
			System.out.println("Connecting to server: " + host);
			connect(host, port);
			System.out.println("Connected!");
			System.out.println("Use commands GET <local pathname> <foreign pathname>, PUT <local pathname> <foreign pathname>, NOOP or QUIT");
			while ((userInput = stdIn.readLine()) != null) {
				// user input must be get, put or quit
				uInputSplit = userInput.split(" ", 4);
				if (uInputSplit[0].equalsIgnoreCase("GET")) {
					get(uInputSplit);
				} else if (uInputSplit[0].equalsIgnoreCase("PUT")) {
					put(uInputSplit);
				} else if (uInputSplit[0].equalsIgnoreCase("QUIT")) {

					disconnect();
				} else if (uInputSplit[0].equalsIgnoreCase("NOOP")) {
					send("NOOP");
					getResponse();
				} else {
					System.err
							.println("Incorrect user input, command must be either GET, PUT or QUIT");
				}

			}

		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void get(String[] uInputSplit) throws Exception {
		// get <local_pathname> <foreign_pathname>
		send("TYPE I");
		getResponse();
		if (uInputSplit[1] != null && uInputSplit[2] != null) {
			String portCommand = getPortCommandFormat();
			send(portCommand);
			String resp = getResponse();
			if (resp.startsWith("2")) {
				// server accepted the port command
				send("RETR " + uInputSplit[2]);
				resp = getResponse();
				if (resp.startsWith("1")) {
					dataSocket = serverSocketData.accept();
					getData(uInputSplit[1]);
				} else {
					System.err.println("Error retrieving file");
				}
			} else {
				// server didn't like the port command
				System.err.println("Server did not accept PORT command");
			}

		} else {
			System.err
					.println("User Input Error: GET command must be of the form: GET <local_pathname> <foreign_pathname>");
		}

	}

	public void getData(String localName) throws Exception {
		int bytesRead;
		int current = 0;
		FileOutputStream fos = null;
		BufferedOutputStream bos = null;
		// receive file
		byte[] myByteArray = new byte[500000000];
		InputStream is = dataSocket.getInputStream();
		fos = new FileOutputStream(localName);
		bos = new BufferedOutputStream(fos);
		bytesRead = is.read(myByteArray, 0, myByteArray.length);
		current = bytesRead;

		do {
			bytesRead = is.read(myByteArray, current,
					(myByteArray.length - current));
			if (bytesRead >= 0) {
				current += bytesRead;
			}
		} while (bytesRead > -1);

		bos.write(myByteArray, 0, current);
		bos.flush();

		System.out.println("File " + localName + " downloaded (" + current
				+ " bytes read)");
		/*
		 * dataReader = new BufferedReader(new InputStreamReader(
		 * dataSocket.getInputStream())); fstream = new FileWriter(localName);
		 * writeToFile = new BufferedWriter(fstream); String dataRead;
		 * while((dataRead=dataReader.readLine())!=null){
		 * writeToFile.write(dataRead); }
		 */

		// wait to get a response from server to close all streams and the
		// socket
		String r = getResponse();
		if (r.startsWith("2")) {
			// writeToFile.close();
			// fstream.close();
			// dataReader.close();
			if (dataSocket != null) {
				dataSocket.close();
				dataSocket = null;
			}
			if (fos != null)
				fos.close();
			if (bos != null)
				bos.close();
			if (is != null)
				is.close();
		}
	}

	public void disconnect() {
		try {
			send("QUIT");
			getResponse();
			commandsIn.close();
			commandsOut.close();
			command.close();
			if (serverSocketData != null) {
				serverSocketData.close();
				serverSocketData = null;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.exit(0);
	}

	public String getResponse() throws Exception {
		StringBuffer response = new StringBuffer();
		String line = commandsIn.readLine();
		response.append(line);
		//System.out.println(line);
		// log.info("FtpClient.getResponse(): #" + line + "#");
		//System.out.println(line.length());
		while (line.charAt(3) == '-') {
			line = commandsIn.readLine();
			response.append("\n");
			response.append(line);
			// log.info("FtpClient.getResponse(): #" + line + "#");
		}
		// log.info("return response");
		System.out.println(response.toString());
		return response.toString();
	}

	/**
	 * Gets the Port attribute of the FtpClient class.
	 * 
	 * @return the Port value
	 */

	public void send(String cmnd) throws IOException {
		/*
		 * for (int i = 0; i < cmnd.length(); i++) {
		 * commandsOut.write(cmnd.charAt(i)); }
		 */
		commandsOut.write(cmnd, 0, cmnd.length());
		commandsOut.write('\r');
		commandsOut.write('\n');
		commandsOut.flush();
	}

	public void put(String[] uInputSplit) throws Exception {
		// put <local pathname> <foreign pathname>
		//System.out.println("put command received: " + uInputSplit [0]+ uInputSplit[1] + uInputSplit[2]);
		send("TYPE I");
		getResponse();
		if (uInputSplit.length == 3) {
			String locPathname = uInputSplit[1];
			String forPathname = uInputSplit[2];
			String portCommand = getPortCommandFormat();
			send(portCommand);
			String response = getResponse();
			if (response.startsWith("2")) {
				// server accepted the port command
				send("STOR " + forPathname);
				response = getResponse();

				if (response.startsWith("1")) {
					dataSocket = serverSocketData.accept();
					putData(locPathname);
				} else {
					System.err.println("Error storing file");
				}
			} else {
				// server didnt like the port command
				System.err.println("Server did not accept PORT command");
			}

		} else {
			System.err
					.println("User Input Error: GET command must be of the form: GET <local_pathname> <foreign_pathname>");
		}
	}

	private void putData(String locPathname) {
		File f = new File(locPathname);
		byte[] byteArray = new byte[(int)f.length()];
		FileInputStream fis;
		BufferedInputStream bis;
		OutputStream dos;
		try {
			fis = new FileInputStream(f);
			bis= new BufferedInputStream(fis);
			bis.read(byteArray,0,byteArray.length);
			dos = dataSocket.getOutputStream();
			dos.write(byteArray, 0, byteArray.length);
			System.out.println("after write, before get response");
			dos.close();
			bis.close();
			fis.close();
			dataSocket.close();
			String resp = getResponse();
			if(!resp.startsWith("2")){
				System.err.println("Error transmitting file");
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println("File not found");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	public String getPortCommandFormat() {
		InetAddress inadd = command.getLocalAddress();
		String localIP = inadd.getHostAddress();
		String IPforPortComm = localIP.replace('.', ',');
		int p1 = dataPort / 256;
		int p2 = dataPort % 256;
		String portCommand = "PORT " + IPforPortComm + "," + p1 + "," + p2;
		System.out.println("portCommand: " + portCommand);
		return portCommand;
	}

}
