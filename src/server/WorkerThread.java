package server;
import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.StringTokenizer;


public class WorkerThread  implements Runnable
{
    Socket command = null;
    BufferedReader insteam;
    PrintWriter outsteam;
    String dir = "/home/ryan/Uni/yr3/Sem2/Networks/Storage/";

    public WorkerThread(Socket sock)
    {
        command = sock;
    }

    @Override
    public void run()
    {
        Socket data = null;

        int dataPort = 0;
        InetAddress dataAddr = null;
        try {
            insteam = new BufferedReader(new InputStreamReader(command.getInputStream()));
            outsteam = new PrintWriter(command.getOutputStream(), true);

            setConnection();
            String[] commands;

            while ((commands = getComm()) != null)
            {
                if (commands[0].equals("QUIT"))
                {
                    closeSocket(command);
                    break;
                }

                else if (commands[0].equals("RETR"))
                {
                    try{
                        //open data socket and transfer data from file
                        String filename = commands[1];
                        String filePath = dir.concat(filename);
                        File filew = new File(filePath);
                        byte [] byteArray = new byte[(int)filew.length()];
                        FileInputStream fInsteam = new FileInputStream(filew);
                        BufferedInputStream bInsteam = new BufferedInputStream(fInsteam);
                        bInsteam.read(byteArray, 0, byteArray.length);


                        reply("150 Opening Data connection");

                        data = openDataConnection(dataAddr, dataPort);

                        System.out.println("sending file " + filename + "\t" + byteArray.length + " bytes\n");

                        OutputStream dOutsteam = data.getOutputStream();
                        dOutsteam.write(byteArray, 0, byteArray.length);

                        dOutsteam.flush();
                        dOutsteam.close();
                        bInsteam.close();

                        closeData(data);
                    }catch (FileNotFoundException e){
                        e.printStackTrace();
                        reply("550 File not found. Transfer canceled");
                    }
                }
                else if (commands[0].equals("STOR"))
                {
                    //open data socket and transfer data from file

                    try{
                        String filepath = dir.concat(commands[1]);
                        System.out.println("writing file to " + filepath);
                        FileOutputStream fOutsteam = new FileOutputStream(filepath);
                        BufferedOutputStream bOutsteam = new BufferedOutputStream(fOutsteam);

                        reply("150 Opening Data connection");

                        data = openDataConnection(dataAddr, dataPort);

                        InputStream is = data.getInputStream();

                        int bufferSize = data.getReceiveBufferSize();

                        byte [] byteArray = new byte[bufferSize];

                        int count;

                        while((count = is.read(byteArray)) > 0)
                        {
                            bOutsteam.write(byteArray, 0, count);
                        }
                        bOutsteam.flush();
                        bOutsteam.close();
                        is.close();
                        closeData(data);
                    }catch(FileNotFoundException e){
                        reply("550 File not found. Transfer canceled");
                    }

                }
                else if (commands[0].equals("MODE"))
                {
                    if(commands[1].equals("S"))
                    {
                        reply("200 Transfer Mode - Stream is acceptable on this server");
                    }
                    else if (commands[1].equals("B"))
                    {
                        reply("504 Transfer Mode - Block is not accepted on this server. Only Stream is Accepted");
                    }
                    else if (commands[1].equals("C"))
                    {
                        reply("504 Transfer Mode - Compressed is not accepted on this server. Only Stream is Accepted");
                    }
                }
                else if (commands[0].equals("TYPE"))
                {
                    if (commands[1].equals("A"))
                    {
                        if (commands[2].equals("N"))
                        {
                            reply("200 Type - ASCII Non-print is acceptable on this server");
                        }
                        else if (commands[2].equals("T"))
                        {
                            reply("504 Type - ASCII Telnet is not accepted on this server. Only ASCII Non-print is Accepted");
                        }
                        else if (commands[2].equals("C"))
                        {
                            reply("504 Type - ASCII Carriage Control is not accepted on this server. Only ASCII Non-print is Accepted");
                        }
                    }
                    else if (commands[1].equals("E"))
                    {
                        if (commands[2].equals("N"))
                        {
                            reply("504 Type - EBCDIC Non-print is not accepted on this server. Only ASCII Non-print is Accepted");
                        }
                        else if (commands[2].equals("T"))
                        {
                            reply("504 Type - EBCDIC Telnet is not accepted on this server. Only ASCII Non-print is Accepted");
                        }
                        else if (commands[2].equals("C"))
                        {
                            reply("504 Type - EBCDIC Carriage Control is not accepted on this server. Only ASCII Non-print is Accepted");
                        }
                    }
                    else if (commands[1].equals("I"))
                    {
                        reply("200 Type - Binary is acceptable on this server");
                    }
                    else if (commands[1].equals("L"))
                    {
                        reply("504 Type - Local Byte is not accepted on this server. Only ASCII Non-print is Accepted");
                    }
                }
                else if (commands[0].equals("STRU"))
                {
                    if(commands[1].equals("F"))
                    {
                        reply("200 File Structure - File is acceptable on this server");
                    }
                    else if (commands[1].equals("R"))
                    {
                        reply("504 File Structure - Record is not accepted on this server");
                    }
                    else if (commands[1].equals("P"))
                    {
                        reply("504 File Structure - Page is not accepted on this server");
                    }
                }
                else if (commands[0].equals("PORT"))
                {
                    String[] addr = getPort(commands[1]);
                    String ip = addr[0];
                    for (int i = 1; i<4; i++)
                    {
                        ip += ".";
                        ip += addr[i];
                    }
                    dataAddr = InetAddress.getByName(ip);
                    dataPort = 256*(Integer.parseInt(addr[4]));
                    dataPort += Integer.parseInt(addr[5]);
                    reply("200 IP and Port number received");

                }
                else if (commands[0].equals("NOOP"))
                {
                    reply("200 Everything is okay");
                }
                else if (commands[0].equals("PASS"))
                {
                    reply("202 Command not necessary on this server");
                }
                else if(commands[0].endsWith("ABOR"))
                {
                    reply("226 File transfer already complete");
                }
                else
                {
                    System.out.println(commands[0] + "\n" +
                            "was received from the client but is undefined");
                    reply("502 Command not implemented");
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Socket openDataConnection(InetAddress addr, int portnum) {
        try{
            return new Socket(addr, portnum);

        }catch(IOException e){
            e.printStackTrace();
            reply("425 Can't open data connection.");
        }
        return null;
    }

    private String[] getPort(String s)
    {
        return s.split(",");
    }

    public String[] getComm()
    {
        try {

            String com = insteam.readLine();
            System.out.println("Received from the client - " + com);
            String[] toks = com.split("\\s");
            toks[0] = toks[0].toUpperCase();
            return toks;

        } catch (IOException e){
            e.printStackTrace();
        }
        System.out.println("An unexpected error occurred when trying to read a command see getComm function");
        return null;
    }

    public void reply(String s)
    {
        s += "\r";
        outsteam.println(s);
        System.out.println(s + "\n" +
                "Was sent to the client as a reply \n");
    }

    public boolean setConnection()
    {
//        System.out.println("entered setConnection");
        int stepnum = 0;
        String[] commands;
        reply("220 Service is ready please login"); //Tell client service is ready for use
        while(stepnum < 1)
        {
//            System.out.println("Entered login while loop");
            commands = getComm();
            if (commands[0].equals("USER"))
            {
                reply("230 username accepted, you are now logged in");
                stepnum++;
            }
            else
            {
                reply("530 You haven't logged in yet");
            }
        }
        return true;
    }

    public void closeSocket(Socket s)
    {
        System.out.println("Entered close connection");
        try{
            reply("221 Service closing control connection");
            insteam.close();
            outsteam.close();
            s.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeData(Socket s)
    {
        System.out.println("entered close Data connection");
        try{
            reply("226 File transfer completed, Closing data connection now.");
            s.close();
        }catch(IOException e){
            e.printStackTrace();
        }
    }
}