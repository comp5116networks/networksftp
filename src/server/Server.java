package server;

import java.io.*;
import java.net.*;

public class Server {
    public static void main(String [] args) throws IOException
    {
        ServerSocket server = new ServerSocket(21);

        while (true)
        {
            Socket client = server.accept();
            WorkerThread w = new WorkerThread(client);
            new Thread(w).start();
        }
    }
}
