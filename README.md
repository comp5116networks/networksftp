# COMP5116 Design of Networks & Distributed Systems #


## Semester 2, 2014 


## Programming Assignment 1 ##

Write your own server and client to implement a subset of the RFC 959 File Transfer Protocol (FTP).  
This assignment may be written in either Java or other languages that can run on SIT machines for
demonstration. By the end of this assignment, you should know how to write a multi‐threaded TCP
socket program and how to comply to a defined protocol.

**Part 1 **

Implement a FTP client capable of connecting to ftp://mirror.aarnet.edu.au and downloading files
from there. This client must be command line based and user must be able to enter GET command
repeatedly until CLOSE/QUIT is called.

**Part 2 **

Implement a FTP server. You may choose to implement an ACTIVE mode or PASSIVE mode server.  
Test your server by connecting from a simple command‐line based FTP client such as:

* Microsoft Windows: Use the already provided ftp.exe in the command prompt.

* UNIX or Mac: Use the ftp command in the console.

The client must be able to connect, upload and download files from the server. Do not use your
client from Part 1.

**Part 3 **

Now add the upload functionality (PUT command) to your client in Part 1. Test your client with the
FTP server from Part 2.
__________________________________________________________________________________

**Notes:** 

* For both parts, you only need to implement the most minimal functionalities to get it working. Amongst these are session establishment, download and upload functions. Other functionalities such as directory listings (LS) are optional.
* The FTP server is a multi‐threaded application listening on port 21 for requests.  The server creates a server socket and loops to accept client connections. When the server starts, it listens for connections on the FTP server port and spawns a new thread to handle each connection. A separate data socket is created to transfer the data, with one TCP connection established per file transfer. The transferred data could be any file for testing purposes.
* You must write the FTP functionalities from scratch and not use any third‐party FTP libraries.
* Observe the replies message from client/server to know if you implemented a command wrongly.